import java.awt.image.BufferedImage;

import java.io.File;
import javax.imageio.ImageIO;

import java.io.IOException;

import java.util.List;
import java.util.ArrayList;

import java.awt.Desktop;

public class FileFunctions
{
    BufferedImage readImageFromFile(String filename) throws IOException
    {
        BufferedImage img = null;
        img = ImageIO.read(new File(filename));
        return img;
    }

    List<HueColor> imageToHueColorArray(BufferedImage img, int width, int height)
    {
        List<HueColor> arr = new ArrayList<HueColor>();
        for (int w=0; w<width; w++)
            for (int h=0; h<height; h++)
                arr.add(new HueColor(img.getRGB(w, h)));
        return arr;
    }

    void writeImageToFile(BufferedImage img, String filename) throws IOException
    {
        ImageIO.write(img, "png", new File(filename));
    }

    void writeHueColorArrayToFile(List<HueColor> arr, int width, int height, String filename) throws IOException
    {
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int w=0; w<width; w++)
           for (int h=0; h<height; h++)
              img.setRGB(w, h, arr.get(h*width + w).getRGB());
        ImageIO.write(img, "png", new File(filename));
    }

    void openImage(String filename) throws IOException
    {
        Desktop.getDesktop().open(new File(filename));
    }
}