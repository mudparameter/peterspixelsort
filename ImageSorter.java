import java.io.IOException;
import java.awt.image.BufferedImage;

import java.util.Random;

public class ImageSorter
{
    private BufferedImage base;
    private int width, height;

    private int intFloor = 0;
    private int intCeil = 10;

    private int jumpSize = 10;

    private int hueFloor = 0;
    private int hueCeil = 100;
    private int satFloor = 0;
    private int satCeil = 100;
    private int valFloor = 0;
    private int valCeil = 100;

    private boolean pixelQualifies(HueColor col)
    {
        if (col.getHue() >= this.hueFloor && col.getHue() <= this.hueCeil)
            if (col.getBrightness() >= this.valFloor && col.getBrightness() <= valCeil)
                if (col.getSaturation() >= this.satFloor && col.getSaturation() <= this.satCeil)
                    return true;
        return false;
    }

    public ImageSorter()
    {
        base = null;
    }

    public ImageSorter(BufferedImage base)
    {
        this.base = base;
        this.width = this.base.getWidth();
        this.height = this.base.getHeight();
    }

    public BufferedImage getBase()
    {
        return this.base;
    }

    public void setNull()
    {
        this.base = null;
        this.width = 0;
        this.height = 0;
    }

    public void setBase(BufferedImage image)
    {
        this.base = image; 
        this.width = image.getWidth();
        this.height = image.getHeight();
    }

    private int normalizeIntensity(int intensity)
    {
        if (intensity > 500)
            return 500;
        return intensity;
    }

    public BufferedImage TranslateVertical(int intFloor, int intCeil, int jumpSize, int hzf, int hzc, int szf, int szc, int bzf, int vzf)
    {
        this.intFloor = normalizeIntensity(intFloor);
        this.intCeil = normalizeIntensity(intCeil);

        this.jumpSize = jumpSize;

        this.hueFloor = hzf;
        this.hueCeil = hzc;

        this.satFloor = szf;
        this.satCeil = szc;

        this.valFloor = bzf;
        this.valCeil = vzf;

        for (int w=0; w<this.width; w++)
        {
            for (int h=0; h<this.height; h+=jumpSize)
            {
                HueColor col = new HueColor(this.base.getRGB(w, h));

                if (pixelQualifies(col))
                    this.verticalPixelLengthen(w, h);
            }
        }
        return this.base;
    }

    private void verticalPixelLengthen(int x, int y)
    {
        Random rand = new Random();
        HueColor col = new HueColor(this.base.getRGB(x, y));

        int intensity = rand.nextInt(this.intCeil - this.intFloor + 1) + this.intFloor;
  
        for (int offset=y; offset<y+intensity; offset++)
        {
            if (offset == this.height-1)
                break;
            this.base.setRGB(x, offset, col.getRGB());
        }
    }

    public BufferedImage TranslateHorizontal(int intFloor, int intCeil, int hzf, int hzc, int szf, int szc, int bzf, int vzf)
    {
        this.intFloor = normalizeIntensity(intFloor);
        this.intCeil = normalizeIntensity(intCeil);

        this.hueFloor = hzf;
        this.hueCeil = hzc;

        this.satFloor = szf;
        this.satCeil = szc;

        this.valFloor = bzf;
        this.valCeil = vzf;

        for (int h=0; h<this.height; h++)
        {
            for (int w=0; w<this.width; w+=10)
            {
                HueColor col = new HueColor(this.base.getRGB(w, h));
                if (pixelQualifies(col))
                    this.horizontalPixelLengthen(w, h);
            }
        }
        return this.base;
    }

    private void horizontalPixelLengthen(int x, int y)
    {
        Random rand = new Random();
        HueColor col = new HueColor(this.base.getRGB(x, y));

        int intensity = rand.nextInt(this.intCeil - this.intFloor + 1) + this.intFloor;
  
        for (int offset=x; offset<x+intensity; offset++)
        {
            if (offset == this.width-1)
                break;
            this.base.setRGB(offset, y, col.getRGB());
        }
    }

    public BufferedImage TranslateDiagonal(int intFloor, int intCeil, int hzf, int hzc, int szf, int szc, int bzf, int vzf)
    {
        this.intFloor = normalizeIntensity(intFloor);
        this.intCeil = normalizeIntensity(intCeil);

        this.hueFloor = hzf;
        this.hueCeil = hzc;

        this.satFloor = szf;
        this.satCeil = szc;

        this.valFloor = bzf;
        this.valCeil = vzf;

        for (int w=0; w<this.width; w++)
        {
            for (int h=0; h<this.height; h+=10)
            {
                HueColor col = new HueColor(this.base.getRGB(w, h));
                if (pixelQualifies(col))
                    this.diagonalPixelLengthen(w, h);
            }
        }
        return this.base;
    }

    private void diagonalPixelLengthen(int x, int y)
    {
        Random rand = new Random();
        HueColor col = new HueColor(this.base.getRGB(x, y));

        int intensity = rand.nextInt(this.intCeil - this.intFloor + 1) + this.intFloor;
        intensity /= 2;
  
        for (int offsetY=y, offsetX=x; offsetY<y+intensity; offsetY++, offsetX++)
        {
            if (offsetX == this.width-1 || offsetY == this.height-1)
                break;
            this.base.setRGB(offsetX, offsetY, col.getRGB());
        }
    }

}