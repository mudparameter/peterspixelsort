# PetersPixelSort

a image pixel sorter with a GUI written in java and java swing

to run: 

1. clone repo and enter cloned directory

    `git clone https://gitlab.com/mudparameter/peterspixelsort`
    
    `cd peterspixelsort`

2. compile PSGUIController.java, i.e.:

    `javac PSGUIController.java`

3. run dat: 

    `java PSGUIController`
