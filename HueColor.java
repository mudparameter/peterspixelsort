import java.awt.Color;
import java.lang.Comparable;

// this is a subclass of the default awt.Color class that implements Comparable for sorting and 
// contains a getHue function that allows for color comparison
class HueColor extends Color implements Comparable<HueColor>
{
   static final long serialVersionUID = 0;
   private float [] hsv = new float [3];

   HueColor(int r, int g, int b)
   {
      super(r, g, b);
      RGBtoHSB(getRed(), getGreen(), getBlue(), this.hsv);
   }

   HueColor(int rgb)
   {
      super(rgb);
      RGBtoHSB(getRed(), getGreen(), getBlue(), this.hsv);
   }

   private float [] getHSBVals()
   {
      return this.hsv;
   }

   // get hue method uses the inbuilt HSB func and multiplies the hue val
   int getHue()
   {
      return (int)(this.hsv[0]*100);
   }

   int getSaturation()
   {
      return (int)(this.hsv[1]*100);
   }

   int getBrightness()
   {
      return (int)(this.hsv[2]*100);
   }

   int getHueZone()
   {
      int hue = getHue();
      int extra = hue % 100;
      return hue - extra;
   }

   // uncomment and comment the different components of this method 
   // to acheive different result images. 
   // TODO: will add switch functionality, maybe with Comparator instead of Comparable interface???
   public int compareTo(HueColor a)
   {
      return this.getHue() - a.getHue();
      //return this.getSaturation() - a.getSaturation();
      //return this.getBrightness() - a.getBrightness();
   }
}