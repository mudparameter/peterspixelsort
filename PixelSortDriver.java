import java.io.IOException;
import java.awt.image.BufferedImage;

import java.io.File;

import java.util.Arrays;

// this file is used to do cool stuff to images via the cli
// TODO: needs updating before it can be used as methods have changed, see line 92

// TODO: seed the random object and instantiate it at a higher point in the stack,
// to allow for a controlled lengthening and shortening of the pixels trails ya dig

// TODO: or just change the max and min of the pixels trails as we go in order to 
// make a "cool - ass effect"

public class PixelSortDriver
{
    public static void main(String [] args)
    {
        String oneOrDir = args[0]; // can be either one or dir
        String infile = args[1];
    
        if (oneOrDir.equals("dir"))
        {
            int function = Integer.valueOf(args[2]);
            int intensity = Integer.valueOf(args[3]);
            int huezonefloor = Integer.valueOf(args[4]);
            int huezoneceil = Integer.valueOf(args[5]);

            final File folder = new File(infile);   // declaring the passed foldername as a new File obj

            File [] files = folder.listFiles();
            Arrays.sort(files);

            boolean inc = true;

            int counter = 0;
            for (final File fileEntry : files) // for each File filename inside that foldername
            {
                String currentFile = infile + "/" + fileEntry.getName(); // the name of the file we'll be writing and reading
                
                if (intensity > 50) inc = false;
                else if (intensity < 4) inc = true;
                if (inc) 
                {
                    if (counter % 3 == 0) intensity++;
                }
                else if (counter % 3 == 0) intensity --;

                performSort(currentFile, "", function, intensity, huezonefloor, huezoneceil);
                counter++;
            }
        }
        else 
        {
            String outfile = args[2];
            int function = Integer.valueOf(args[3]);
            int intensity = Integer.valueOf(args[4]);
            int huezonefloor = Integer.valueOf(args[5]);
            int huezoneceil = Integer.valueOf(args[6]);

            FileFunctions filefuncs = new FileFunctions();  // instantiating func object here to prevent overhead

            performSort(infile, outfile, function, intensity, huezonefloor, huezoneceil);

            try {
                filefuncs.openImage(outfile);
            } catch(IOException e) {
                System.out.println("Couldn't open image file.");
            }
        }
    }

    static void performSort(String infile, String outfile, int function, int intensity, int huezonefloor, int huezoneceil)
    {
        if (outfile.isEmpty()) outfile = infile;

        FileFunctions filefuncs = new FileFunctions();  // instantiating func object here to prevent overhead
        
        BufferedImage img = null;
        try {
            img = filefuncs.readImageFromFile(infile);
        } catch (IOException e) {
            System.out.println("Error on image read for " + infile + ". Quitting...");
            System.exit(1);
        }

        ImageSorter sorter = new ImageSorter(img);
        System.out.println("Sorting image " + infile + " now...");

        // TODO: fix dis shiiiiiii
        /*
        // add effects to upon each other for a cooler effect
        switch (function)
        {
            case 0: 
                img = sorter.TranslateVertical(intensity, huezonefloor, huezoneceil);
                break;
            case 1: 
                img = sorter.TranslateHorizontal(intensity, huezonefloor, huezoneceil);
                break;
            case 2:
                img = sorter.TranslateDiagonal(intensity, huezonefloor, huezoneceil);
                break;
            case 3:
                img = sorter.TranslateVertical(intensity, huezonefloor, huezoneceil);
                img = sorter.TranslateHorizontal(intensity, huezonefloor, huezoneceil);
                img = sorter.TranslateDiagonal(intensity, huezonefloor, huezoneceil);
                break;
            default: 
                System.out.println("Invalid sort option.");
                System.exit(1);
        }
        */

        try {      
            filefuncs.writeImageToFile(img, outfile);
        } catch (IOException e) {
            System.out.println("Image write fail for " + outfile + ". Quitting...");
            System.exit(1);
        }
    }
}