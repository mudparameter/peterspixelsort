import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

// TODO: increase efficiency of sorting algorithm by making sure we arent setting pixels that are already a qual. color
// TODO: tried doing the above and broke my brain. oops. maybe another time. 

public class PSGUIController implements ActionListener
{
    static final long serialVersionUID = 69; // why is this a requirement.

    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    // these are the image variables
    private int scale = 600;

    private BufferedImage original;
    private BufferedImage sorted;

    ImageSorter sorter = new ImageSorter();

    private boolean imageLoaded = false;
    private boolean imageSorted = false;

    // main frame
    private JFrame mainFrame;

    // main panel: what all sub panels are added to
    private JPanel mainPanel;

    // here begin the file selection components
    private JPanel fcPanel;
    private JButton openButton;
    private JLabel selectedFileNameLabel;

    // here begin the image panel components
    private JPanel imagePanel;
    private JLabel selectedImageLabel;
    private JLabel sortedImageLabel;

    // here begin the control components
    private JPanel inputPanel;

    // these sub panels allow for organization 
    private JPanel inputSubPanel1;
    private JPanel inputSubPanel2;
    private JPanel inputSubPanel3;
    private JPanel inputSubPanel4;

    private JLabel functionLabel;
    private JComboBox<String> functionComboBox;

    private JLabel jumpSizeLabel;
    private JTextField jumpSizeField;

    private int inputWidth = 3;

    private JLabel intensityFloorLabel;
    private JTextField intensityFloorField;

    private JLabel intensityCeilLabel;
    private JTextField intensityCeilField;

    private JLabel hueZoneFloorLabel;
    private JTextField hueZoneFloorField;

    private JLabel hueZoneCeilLabel;
    private JTextField hueZoneCeilField;

    private JLabel satZoneFloorLabel;
    private JTextField satZoneFloorField;

    private JLabel satZoneCeilLabel;
    private JTextField satZoneCeilField;

    private JLabel valZoneFloorLabel;
    private JTextField valZoneFloorField;

    private JLabel valZoneCeilLabel;
    private JTextField valZoneCeilField;

    // here are the submit and write buttons
    private JButton sort;
    private JButton reset;
    private JButton save;

    // here are the status components
    private JPanel statusPanel;
    private JLabel statusLabel;

    PSGUIController() 
    {
        this.prepareGUI();
    }

    private void prepareGUI()
    {
        mainFrame = new JFrame("Peters Pixel Sorter");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

        addFileChooser(); // adds the file chooser and the filename display panel to main panel
        mainPanel.add(fcPanel);

        addImageDisplay(); // adds the image display panel to main panel
        mainPanel.add(imagePanel);

        addInputComponents(); // adds the user control components panel to main panel
        mainPanel.add(inputPanel);

        addStatusComponents();
        mainPanel.add(statusPanel);

        mainFrame.add(mainPanel);

        // TODO: make the scrollbars work without resizing upon every action
        // TODO: maybe take a look at the updateDisplay()?

        //JScrollPane pane = new JScrollPane(mainPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        //mainFrame.setContentPane(pane);

        mainFrame.setResizable(true);

        mainFrame.pack();
    }

    private void updateDisplay() // must be called after changing content to reflect changes on display
    {
        mainFrame.revalidate();
        mainFrame.pack();
    }

    private void addFileChooser()
    {
        fcPanel = new JPanel();

        selectedFileNameLabel = new JLabel("Selected image will appear below.");

        openButton = new JButton("Open an image...");
        openButton.addActionListener(this);

        fcPanel.add(openButton);
        fcPanel.add(selectedFileNameLabel);
    }

    private void addImageDisplay()
    {
        imagePanel = new JPanel();

        selectedImageLabel = new JLabel();
        imagePanel.add(selectedImageLabel);

        sortedImageLabel = new JLabel();
        imagePanel.add(sortedImageLabel);
    }

    private void addInputComponents()
    {
        inputPanel = new JPanel();
        inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.Y_AXIS));

        // FUNCTION INPUT FIELDS
        inputSubPanel1 = new JPanel();

        functionLabel = new JLabel("Function:");
        String [] functions = {"Down", "Right", "Diagonal"};
        functionComboBox = new JComboBox<>(functions);
        functionComboBox.setSelectedIndex(0);

        inputSubPanel1.add(functionLabel);
        inputSubPanel1.add(functionComboBox);

	jumpSizeLabel = new JLabel("Jump size:");
	jumpSizeField = new JTextField("10");
	jumpSizeField.setColumns(inputWidth);

	inputSubPanel1.add(jumpSizeLabel);
	inputSubPanel1.add(jumpSizeField);

        intensityFloorLabel = new JLabel("Intensity floor:");
        intensityFloorField = new JTextField("0");
        intensityFloorField.setColumns(inputWidth);

        inputSubPanel1.add(intensityFloorLabel);
        inputSubPanel1.add(intensityFloorField);

        intensityCeilLabel = new JLabel("Intensity ceil:");
        intensityCeilField = new JTextField("50");
        intensityCeilField.setColumns(inputWidth);

        inputSubPanel1.add(intensityCeilLabel);
        inputSubPanel1.add(intensityCeilField);

        // HUE INPUT FIELDS
        hueZoneFloorLabel = new JLabel("Hue Zone Floor:");
        hueZoneFloorField = new JTextField("0");
        hueZoneFloorField.setColumns(inputWidth);

        inputSubPanel1.add(hueZoneFloorLabel);
        inputSubPanel1.add(hueZoneFloorField);

        hueZoneCeilLabel = new JLabel("Hue Zone Ceil:");
        hueZoneCeilField = new JTextField("100");
        hueZoneCeilField.setColumns(inputWidth);

        inputSubPanel1.add(hueZoneCeilLabel);
        inputSubPanel1.add(hueZoneCeilField);

        inputPanel.add(inputSubPanel1);

        // SATURATION INPUT FIELDS
        inputSubPanel2 = new JPanel();

        satZoneFloorLabel = new JLabel("Saturation Zone Floor:");
        satZoneFloorField = new JTextField("0");
        satZoneFloorField.setColumns(inputWidth);

        inputSubPanel2.add(satZoneFloorLabel);
        inputSubPanel2.add(satZoneFloorField);

        satZoneCeilLabel = new JLabel("Saturation Zone Ceil:");
        satZoneCeilField = new JTextField("100");
        satZoneCeilField.setColumns(inputWidth);

        inputSubPanel2.add(satZoneCeilLabel);
        inputSubPanel2.add(satZoneCeilField);

        // BRIGHTNESS VALUE FIELDS
        valZoneFloorLabel = new JLabel("Brightness Zone Floor:");
        valZoneFloorField = new JTextField("0");
        valZoneFloorField.setColumns(inputWidth);

        inputSubPanel2.add(valZoneFloorLabel);
        inputSubPanel2.add(valZoneFloorField);

        valZoneCeilLabel = new JLabel("Brightness Zone Ceil:");
        valZoneCeilField = new JTextField("100");
        valZoneCeilField.setColumns(inputWidth);

        inputSubPanel2.add(valZoneCeilLabel);
        inputSubPanel2.add(valZoneCeilField);

        inputPanel.add(inputSubPanel2);

        // CONTROL BUTTON INPUT FIELDS
        inputSubPanel4 = new JPanel();

        sort = new JButton("Sort");
        sort.addActionListener(this);
        inputSubPanel4.add(sort);

        reset = new JButton("Reset");
        reset.addActionListener(this);
        inputSubPanel4.add(reset);

        save = new JButton("Save as...");
        save.addActionListener(this);
        inputSubPanel4.add(save);

        inputPanel.add(inputSubPanel4);
    }

    private void addStatusComponents()
    {
        statusPanel = new JPanel();
        statusLabel = new JLabel();
        setStatusText("Enter settings and hit sort when ready.");
        statusPanel.add(statusLabel);
    }

    private boolean validateInput()
    {
        String field = "";

        field = intensityFloorField.getText();
        if (field.isEmpty() || !isNumeric(field))
        {
            setErrorText("Error: invalid input for intensity floor.");
            return false;
        }

        field = intensityCeilField.getText();
        if (field.isEmpty() || !isNumeric(field))
        {
            setErrorText("Error: invalid input for intensity ceiling.");
            return false;
        }

        field = jumpSizeField.getText();
        if (field.isEmpty() || !isNumeric(field)) 
        {
            setErrorText("Error: invalid input for jump size");
            return false;
        }

        field = hueZoneFloorField.getText();
        if (field.isEmpty() || !isNumeric(field))
        {
            setErrorText("Error: invalid input for hue zone floor.");
            return false;
        }

        field = hueZoneCeilField.getText();
        if (field.isEmpty() || !isNumeric(field))
        {
            setErrorText("Error: invalid input for hue zone ceiling.");
            return false;
        }

        field = satZoneFloorField.getText();
        if (field.isEmpty() || !isNumeric(field))
        {
            setErrorText("Error: invalid input for sat zone floor.");
            return false;
        }

        field = satZoneCeilField.getText();
        if (field.isEmpty() || !isNumeric(field))
        {
            setErrorText("Error: invalid input for sat zone ceiling.");
            return false;
        }

        field = valZoneFloorField.getText();
        if (field.isEmpty() || !isNumeric(field))
        {
            setErrorText("Error: invalid input for val zone floor.");
            return false;
        }

        field = valZoneCeilField.getText();
        if (field.isEmpty() || !isNumeric(field))
        {
            setErrorText("Error: invalid input for val zone ceiling.");
            return false;
        }

        return true;
    }

    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == openButton)
        {
            JFileChooser fileChooser = new JFileChooser();

            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Images", "jpeg", "jpg", "png", "bmp", "tiff", "gif"));

            int returnVal = fileChooser.showOpenDialog(mainPanel);

            if (returnVal == JFileChooser.APPROVE_OPTION) 
            {
                File selectedFile = fileChooser.getSelectedFile();
                selectedFileNameLabel.setText("Selected file: " + selectedFile.getName());

                try {
                    original = ImageIO.read(selectedFile);
                    selectedImageLabel.setIcon(scaleImage(new ImageIcon(original), scale, scale));
                    imageLoaded = true;
                    imageSorted = false;
                    setStatusText("Image loaded.");
                } catch (IOException ex) {
                    setErrorText("Error on image load.");                    
                }

                sorted = deepCopy(original);
                sorter.setBase(sorted);

                sortedImageLabel.setIcon(new ImageIcon());

                updateDisplay();
            }
        }
        else if (e.getSource() == sort) 
        {            
            // if input is invalid or if there is no image loaded, error
            if (!validateInput()) return; 
            if (!imageLoaded) 
            {
                setErrorText("Error: no image loaded.");
                return;
            }

            String function = (String)functionComboBox.getSelectedItem();

            int jumpSize = Integer.valueOf(jumpSizeField.getText());

            int intensityFloor = Integer.valueOf(intensityFloorField.getText());
            int intensityCeil = Integer.valueOf(intensityCeilField.getText());

            int hueZoneFloor = Integer.valueOf(hueZoneFloorField.getText());
            int hueZoneCeil = Integer.valueOf(hueZoneCeilField.getText());

            int satZoneFloor = Integer.valueOf(satZoneFloorField.getText());
            int satZoneCeil = Integer.valueOf(satZoneCeilField.getText());

            int valZoneFloor = Integer.valueOf(valZoneFloorField.getText());
            int valZoneCeil = Integer.valueOf(valZoneCeilField.getText());

            switch (function)
            {
                case "Down":
                    sorted = sorter.TranslateVertical(intensityFloor, intensityCeil, jumpSize, hueZoneFloor, hueZoneCeil, satZoneFloor, satZoneCeil, valZoneFloor, valZoneCeil);
                    break;
                case "Right":
                    sorted = sorter.TranslateHorizontal(intensityFloor, intensityCeil, hueZoneFloor, hueZoneCeil, satZoneFloor, satZoneCeil, valZoneFloor, valZoneCeil);
                    break;
                case "Diagonal":
                    sorted = sorter.TranslateDiagonal(intensityFloor, intensityCeil, hueZoneFloor, hueZoneCeil, satZoneFloor, satZoneCeil, valZoneFloor, valZoneCeil);
                    break;
            }

            sortedImageLabel.setIcon(scaleImage(new ImageIcon(sorted), scale, scale));

            setStatusText("Sorted.");
            imageSorted = true;

            mainFrame.revalidate();
        }
        else if (e.getSource() == reset) 
        {
            if (!imageLoaded || !imageSorted)
            {
                setErrorText("Error: no image to reset.");
                return;
            }

            sorted = deepCopy(original);
            sorter.setBase(sorted);
            sortedImageLabel.setIcon(scaleImage(new ImageIcon(original), scale, scale));
            mainFrame.revalidate();
            //updateDisplay();
        }
        else if (e.getSource() == save)
        {
            if (!imageSorted)
            {
                setErrorText("Error: no sorted image to save.");
                return;
            }

            JFileChooser fileChooser = new JFileChooser();
            int returnVal = fileChooser.showSaveDialog(mainPanel);

            if (returnVal == JFileChooser.APPROVE_OPTION) 
            {
                // TODO: force the user to save as an image file
                // TODO: add multiple image filetype save functionality

                File selectedFile = fileChooser.getSelectedFile();

                try {
                    ImageIO.write(sorted, "png", selectedFile);
                    setStatusText("Image saved successfully.");
                } catch (IOException ex) {
                    System.out.println("oops, error on image save");
                }
            }
        }
    }

    private void setErrorText(String err)
    {
        statusLabel.setForeground(Color.RED);
        statusLabel.setText(err);
        updateDisplay();
    }

    private void setStatusText(String status)
    {
        statusLabel.setForeground(Color.BLACK);
        statusLabel.setText(status);
        updateDisplay();
    }

    // i got this method from GitHub. it's beautiful. thank you Kyle Phillips. 
    // it scales an imageicon, keeping the aspect ratio. 
    public ImageIcon scaleImage(ImageIcon icon, int w, int h)
    {
        int nw = icon.getIconWidth();
        int nh = icon.getIconHeight();

        if(icon.getIconWidth() > w)
        {
          nw = w;
          nh = (nw * icon.getIconHeight()) / icon.getIconWidth();
        }

        if(nh > h)
        {
          nh = h;
          nw = (icon.getIconWidth() * nh) / icon.getIconHeight();
        }

        return new ImageIcon(icon.getImage().getScaledInstance(nw, nh, Image.SCALE_DEFAULT));
    }

    // i got this method from github also. it is also beautiful. thank you user1050755.
    // makes a deep copy of a bufferedimage.
    public static BufferedImage deepCopy(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(bi.getRaster().createCompatibleWritableRaster());
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

    // i got this method from github also. beautiful part 3. thank you CraigTP.
    // checks if a string is numeric or not.
    public static boolean isNumeric(String str)  
    {  
        try {  
            double d = Double.parseDouble(str);  
        }  
        catch(NumberFormatException nfe) {  
            return false;  
        }  
        return true;  
    }

    public void showGUI()
    {
        mainFrame.setVisible(true);
    }

    public static void main(String [] args)
    {
        PSGUIController gui = new PSGUIController();
        gui.showGUI();
    }
}
